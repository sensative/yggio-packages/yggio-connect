// wrap-refresh.test.js

const _ = require('lodash');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const assert = require('assert');

// create proxies for 'require' stuff
const login = sinon.spy(async () => ({login: true}));
const registerClient = sinon.spy(async () => ({register: true}));
const refreshAccessToken = sinon.spy(async (user, refreshToken) => {
  if (refreshToken === 'invalid') {
    throw new Error('refreshAccessToken failed');
  }
  return {refresh: true};
});
const subscribe = sinon.spy(async () => ({subscribe: true}));

const serviceClient = proxyquire('../../src/client', {
  './recursive-login': login,
  './register': registerClient,
  '../logger': {error: () => 0},
  '../routes': {refreshAccessToken, subscribe},
});

describe('client/index (the top level)', () => {
  const initErrorMessage = 'Client has not been initialized';
  describe('pre-initialization', () => {
    it('isInitialized() returns false', () => {
      assert(!serviceClient.isInitialized());
    });
    it('getClientDetails() throws error', () => {
      assert.throws(() => getClientDetails());
    });
    it('subscribe throws initialization error', () => {
      return serviceClient.subscribe()
        .then(res => Promise.reject('should not have succeeded'))
        .catch(err => {
          assert(err.message === initErrorMessage);
        });
    });
    it('wrapRoute throws initialization error', () => {
      assert.throws(() => serviceClient.wrapRoute());
    });
  });

  describe('post-initialization', () => {
    const accountConfig = {};
    const clientConfig = {};
    const refreshCallback = sinon.spy(() => 0);
    const clientAppIdentifiers = {secret: 'secret', client_id: 'test'};
    let entities;
    before(async () => {
      entities = await serviceClient.init(
        accountConfig,
        clientConfig,
        refreshCallback,
        clientAppIdentifiers
      );
    });

    it('isInitialized() returns true', () => {
      assert(serviceClient.isInitialized());
    });

    it('getClientDetails() return the entities', () => {
      const expectedEntities = {
        account: {login: true},
        client: {register: true, ...clientAppIdentifiers}
      };
      const entities = serviceClient.getClientDetails();
      assert(_.isEqual(entities, expectedEntities));
    });

    it('login entities should be well-defined', () => {
      assert(_.isEqual(entities.account, {login: true}));
      assert(_.isEqual(entities.client, {register: true, ...clientAppIdentifiers}));
      assert(login.calledOnce);
      assert(registerClient.calledOnce);
      assert(refreshAccessToken.notCalled);
    });

    it('subscribe should succeed trivially', () => {
      return serviceClient.subscribe()
        .then(res => {
          assert(_.isEqual(res, {subscribe: true}));
          assert(subscribe.calledOnce);
          assert(refreshAccessToken.notCalled);
          subscribe.resetHistory();
        });
    });

    describe('wrap route with refresh --- this one will be tested thoroughly', () => {
      // set up the relevant fake routes
      const validUser = {accessToken: 'xxx', refreshToken: 'xxx'};
      const workingRoute = sinon.spy(async () => ({workingRoute: true}));
      const brokenRoute = sinon.spy(async () => Promise.reject(new Error('General error')));
      const refreshableRoute = sinon.spy(async () => Promise.reject({statusCode: 401}));
      let wrappedWorkingRoute = null;
      let wrappedBrokenRoute = null;
      let wrappedRefreshableRoute = null;
      before(() => {
        wrappedWorkingRoute = serviceClient.wrapRoute(workingRoute);
        wrappedBrokenRoute = serviceClient.wrapRoute(brokenRoute);
        wrappedRefreshableRoute = serviceClient.wrapRoute(refreshableRoute);
      });

      it('workingRoute should succeed (trivially)', () => {
        return wrappedWorkingRoute()
          .then(res => {
            assert(_.isEqual(res, {workingRoute: true}));
            assert(refreshAccessToken.notCalled);
            assert(workingRoute.calledOnce);
            workingRoute.resetHistory();
          });
      });

      it('brokenRoute should fail predictably without valid user', () => {
        return wrappedBrokenRoute()
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.message === 'General error');
            assert(refreshAccessToken.notCalled);
            assert(brokenRoute.calledOnce);
            brokenRoute.resetHistory();
          });
      });

      it('brokenRoute should fail predictably WITH valid user', () => {
        return wrappedBrokenRoute(validUser)
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.message === 'General error');
            assert(refreshAccessToken.notCalled);
            assert(brokenRoute.calledOnce);
            brokenRoute.resetHistory();
          });
      });

      it('refreshableRoute should fail trivially WITHOUT valid user', () => {
        return wrappedRefreshableRoute()
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.statusCode === 401);
            assert(refreshAccessToken.notCalled);
            assert(refreshableRoute.calledOnce);
            refreshableRoute.resetHistory();
          });
      });

      it('refreshableRoute should fail special-like if user.refreshToken is missing', () => {
        return wrappedRefreshableRoute(_.omit(validUser, 'refreshToken'))
        .then(res => Promise.reject(new Error('should have failed')))
        .catch(err => {
          assert(err.message === 'refresh token is missing');
          assert(refreshAccessToken.notCalled);
          assert(refreshableRoute.calledOnce);
          refreshableRoute.resetHistory();
        });
      });

      it('refreshableRoute should return refreshError if refresh fails', () => {
        return wrappedRefreshableRoute(_.assign({}, validUser, {refreshToken: 'invalid'}))
          .then(res => Promise.reject(new Error('should have failed')))
          .catch(err => {
            assert(err.message === 'refreshAccessToken failed');
            assert(refreshAccessToken.calledOnce);
            refreshAccessToken.resetHistory();
            assert(refreshableRoute.calledOnce);
            refreshableRoute.resetHistory();
          });
      });

      it('refreshableRoute should return {statusCode: 401} upon refresh success, but refreshCallback should have been called', () => {
        return wrappedRefreshableRoute(validUser)
        .then(res => Promise.reject(new Error('should have failed')))
        .catch(err => {
          assert(err.statusCode === 401);
          assert(refreshAccessToken.calledOnce);
          refreshAccessToken.resetHistory();
          assert(refreshableRoute.calledTwice); // TWICE!!
          refreshableRoute.resetHistory();
        });
      });
    });
  });

});
