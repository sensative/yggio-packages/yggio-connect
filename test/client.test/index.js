// service-client.test/index.js

describe('testing the service client modules', () => {
  require('./recursive-login.test');
  require('./register.test');
  // and LASTLY test the index with the wrap and setup stuff
  require('./index.test');
});
