// register.test.js

const proxyquire = require('proxyquire');
const sinon = require('sinon');
const assert = require('assert');
const _ = require('lodash');

const registerClient = sinon.spy(async () => ({register: true}));
const setLogo = sinon.spy(async() => {});

const register = proxyquire('../../src/client/register', {
  // we don't want to test the actual routes
  '../routes': {registerClient, setLogo},
  // skip the config validation
  '../config-validator': {assertValidClientConfig: () => 0},
  // and shut up the logger
  '../logger': {log: () => 0, info: () => 0, warn: () => 0, error: () => 0},
});

describe('register.test.js', () => {

  it('should trivially succeed', () => {
    const clientDetails = {
      redirectUris: {},
    };
    return register({serviceAccount: true}, clientDetails)
      .then(res => {
        const expectedRes = {
          register: true,
          redirectUris: {},
        };
        assert(_.isEqual(res, expectedRes));
        assert(registerClient.calledOnce);
        assert(setLogo.notCalled);
        registerClient.resetHistory();
      });
  });

  it('should trivially succeed also with specified logoPath', () => {
    const clientDetails = {
      redirectUris: {},
      logoPath: 'xxx',
    };
    return register({serviceAccount: true}, clientDetails)
      .then(res => {
        const expectedRes = {
          register: true,
          redirectUris: {},
        };
        assert(_.isEqual(res, expectedRes));
        assert(registerClient.calledOnce);
        assert(setLogo.calledOnce);
        registerClient.resetHistory();
        setLogo.resetHistory();
      });
  });
});
