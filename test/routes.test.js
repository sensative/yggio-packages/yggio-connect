// routes.test.js

const _ = require('lodash');
const assert = require('assert');
const proxyquire = require('proxyquire');

const routes = proxyquire('../src/routes', {
  'fs': {
    createReadStream: () => 'fake_file_data',
  },
  './yggio-request': {
    get: async () => [],
    post: async () => 'post',
    put: async () => 'put',
    delete: async () => 'delete',
  }
});


const nonTokenRoutes = [
  'login',
  'register',
  'refreshAccessToken',
  'getEndpoints',
  'oauthCode',
];

const tokenRoutes = [
  // client
  'setLogo',
  'registerClient',
  'fetchClientApp',
  'subscribe',
  // channels
  'getChannels',
  'getChannel',
  'ensureChannel',
  // iotnodes
  'command',
  'getMe',
  'logoutFromYggio',
  'getNode',
  'getNodes',
  'getAccessibleNodes',
  'createNode',
  'updateNode',
  'deleteNode',
  //locations
  'getLocations',
  'updateLocation',
  // oauth
  'checkOAuthStatus',
  // rules
  'getRule',
  'getRules',
  'executeRule',
  'getScenarioTemplates',
  'createScenario',
  'getRulesConditions',
  'updateRulesCondition',
  'createRulesCondition',
  'getRulesActions',
  'createRulesAction',
  'updateRulesAction',
  'deleteRulesAction',
  'createRule',
  'deleteRule',
  'updateRule',
  'addRuleTrigger',
  'addRuleEvent',
  // Contacts
  'getContacts',
  'setContacts',
]
describe('testing routes', () => {

  describe('basic route auditing: do we have the routes we expect?', () => {
    const routeKeys = _.keys(routes);
    const expectedKeys = _.concat([], tokenRoutes, nonTokenRoutes);

    it('should have expected number of routes', () => {
      const numRoutes = routeKeys.length;
      const expectedNumRoutes = expectedKeys.length;
      assert(numRoutes === expectedNumRoutes, `Got ${numRoutes} routes, expected ${expectedNumRoutes}`);
    });

    it('should have no unexpected names of routes', () => {
      const diff = _.difference(routeKeys, expectedKeys);
      assert(!diff.length, 'we have unexpected routes: ' + _.join(diff, ', '));
    });

    it('should have expected names of routes', () => {
      const diff = _.difference(expectedKeys, routeKeys);
      assert(!diff.length, 'we expected but did not find routes: ' + _.join(diff, ', '));
    });
  });

  describe('routes validate input parameters', () => {
    it('all tokenRoutes check for user.accessToken', () => {
      const expectedMessage = 'The user access token is missing (user.accessToken)';
      return Promise.all(_.map(tokenRoutes, routeKey => {
        return routes[routeKey]({})
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(err.message === expectedMessage, routeKey + ' - invalid message: ' + err.message);
          });
      }));
    });

    it('updateRulesAction checks for valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const expectedMessage = 'invalid updates object';
      return routes.updateRulesAction(user)
        .then(() => Promise.reject(new Error('This should have failed')))
        .catch(err => {
          assert(err.message === expectedMessage, 'updateRulesAction - invalid message: ' + err.message);
        });
    });

    it('updateRule checks for valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const expectedMessage = 'invalid updates object';
      return routes.updateRule(user)
        .then(() => Promise.reject(new Error('This should have failed')))
        .catch(err => {
          assert(err.message === expectedMessage, 'updateRule - invalid message: ' + err.message);
        });
    });

    it('login route require "username" and "password" fields', () => {
      const invalids = [{username: 'xxx'}, {password: 'xxx'}];
      const expectedMessage = 'username or password is missing';
      return Promise.all(_.map(invalids, inv => {
        return routes.login(inv)
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(err.message === expectedMessage, 'login - invalid message: ' + err.message);
          });
      }));
    });

    it('register route require "username" and "password" fields', () => {
      const invalids = [{username: 'xxx'}, {password: 'xxx'}];
      const expectedMessage = 'username or password is missing';
      return Promise.all(_.map(invalids, inv => {
        return routes.register(inv)
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(err.message === expectedMessage, 'register - invalid message: ' + err.message);
          });
      }));
    });

    it('refreshAccessToken route requires valid provider and refreshToken', () => {
      const invalids = [
        [{client_id: 'xxx', secret: 'xxx'}, null],
        [{client_id: 'xxx', secret: null}, 'refreshToken'],
        [{client_id: null, secret: 'xxx'}, 'refreshToken'],
      ];
      const expectedMessages = ['client is missing', 'client.client_id is missing', 'client.secret is missing', 'refreshToken is missing'];

      return Promise.all(_.map(invalids, inv => {
        return routes.refreshAccessToken(...inv)
          .then(() => Promise.reject(new Error('This should have failed')))
          .catch(err => {
            assert(_.includes(expectedMessages, err.message), 'register - invalid message: ' + err.message);
          });
      }));
    });
  });

  describe('routes nominally work with correct inputs', () => {
    const specialTokenRoutes = [
      'setLogo',
      'updateRulesAction',
      'updateRule',
      'subscribe',
      'ensureChannel'
    ];
    it('all tokenRoutes (which have no special validation)', () => {
      const user = {accessToken: 'xxx'};
      const nonSpecials = _.difference(tokenRoutes, specialTokenRoutes);
      return Promise.all(_.map(nonSpecials, key => {
        return routes[key](user)
          .catch(err => {
            assert(false, key + ' failed: ' + err.message);
          });
      }));
    });

    it('setLogo needs user, providerId and logoPath', () => {
      const user = {accessToken: 'xxx'};
      const providerId = 'xxx';
      const logoPath = 'xxx';
      return routes.setLogo(user, providerId, logoPath);
    });

    it('updateRulesAction needs user and valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const updates = {_id: 'xxx'};
      return routes.updateRulesAction(user, updates);
    });

    it('updateRule needs user and valid updates object', () => {
      const user = {accessToken: 'xxx'};
      const updates = {_id: 'xxx'};
      return routes.updateRule(user, updates);
    });

    it('subscribe needs a user and valid subscription', () => {
      const user = {accessToken: 'xxx'};
      const subscription = {
        xxx: 'xxx',
        iotnode: 'xxx',
        protocol: 'xxx',
      };
      return routes.subscribe(user, subscription);
    });

    it('login needs valid credentials', () => {
      const credentials = {username: 'xxx', password: 'xxx'};
      return routes.login(credentials);
    });

    it('register needs valid credentials', () => {
      const credentials = {username: 'xxx', password: 'xxx'};
      return routes.register(credentials);
    });
  });
});
