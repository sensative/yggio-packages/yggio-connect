'use strict';
// src/index.js

const assert = require('assert');
const _ = require('lodash');

const logger = require('./logger');
const yggioRequest = require('./yggio-request');
const yggioRoutes = require('./routes');
const serviceClient = require('./client');
const configValidator = require('./config-validator');

// Main initialization of Yggio Connect.
// Establishes connection to the chosen Yggio server and sets up a client if specified.
const init = async (config, clientAppIdentifiers) => {
  assert(config, 'yggio config missing');
  configValidator.assertValidUrl(config);
  yggioRequest.setYggioUrl(config.url);

  const accountDetails = _.get(config, 'account', null);
  const clientDetails = _.get(config, 'client', null);
  const refreshCallback = _.get(config, 'refreshCallback', null);

  if (!accountDetails && !clientDetails && !refreshCallback) {
    logger.warn('initializing yggio-connect WITHOUT client functionality enabled');
    return;
  }

  configValidator.assertValidAccountConfig(accountDetails);
  configValidator.assertValidClientConfig(clientDetails);
  configValidator.assertValidRefreshCallback(refreshCallback);

  return serviceClient.init(accountDetails, clientDetails, refreshCallback, clientAppIdentifiers);
};

const createWrappedRoutes = routes => {
  return _.reduce(routes, (acc, route, key) => {
    let wrappedRoute = null; // lazy load
    acc[key] = async (...args) => {
      if (!serviceClient.isInitialized()) {
        throw new Error('Evaluate wrapped route: service client is not initialized');
      }
      if (!wrappedRoute) {
        wrappedRoute = serviceClient.wrapRoute(routes[key]);
      }
      return wrappedRoute(...args);
    };
    return acc;
  }, {});
};

module.exports = {
  init,
  // the raw routes, exposed for completeness
  rawRoutes: yggioRoutes,
  // the wrapped routes, which should generally be used
  routes: createWrappedRoutes(yggioRoutes),
  // and the client utility functionality
  client: {
    subscribe: serviceClient.subscribe,
    redeemCode: serviceClient.redeemCode,
    getDetails: serviceClient.getClientDetails,
  },
};
