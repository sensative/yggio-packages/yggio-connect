// recursive-login.js
// provides login loop for service-provider.

const _ = require('lodash');

const {login, register} = require('../routes');
const {assertValidAccountConfig} = require('../config-validator');
const logger = require('../logger');

const DEFAULT_RETRY_DELAY = 5000;

const delay = d => new Promise(resolve => setTimeout(resolve, d));

const attemptLogin = async accountInfo => {
  try {
    return await login(accountInfo);
  } catch (e) {
    const retryDelay = _.get(accountInfo, 'retryDelay', DEFAULT_RETRY_DELAY);
    logger.warn(`recursive-login: Register/Login failed: ${e.message}. Retrying in ${retryDelay} ms`);
    await delay(retryDelay);
    return attemptLogin(accountInfo);
  }
};

const acquireAccount = async accountInfo => {
  try {
    return await register(accountInfo);
  } catch (e) {
    logger.warn('recursive-login: failed to register, trying to login instead');
    return attemptLogin(accountInfo);
  }
};

const recursiveLogin = async (accountInfo) => {
  assertValidAccountConfig(accountInfo);
  const serviceAccount = await acquireAccount(accountInfo);
  const account = {...accountInfo, ...serviceAccount};
  logger.info('Successfully logged into account: ', _.get(account, 'username'));
  return account;
};

module.exports = recursiveLogin;
