'use strict';
// register.js

const _ = require('lodash');
const {fetchClientApp, registerClient, setLogo} = require('../routes');
const logger = require('../logger');
const {assertValidClientConfig} = require('../config-validator');

const attemptRegistration = (serviceAccount, clientConfig) => {
  assertValidClientConfig(clientConfig);

  const clientInfo = {
    name: clientConfig.name,
    info: clientConfig.info,
    redirect_uri: Object.values(clientConfig.redirectUris) // works for arrays as well
  };

  return registerClient(serviceAccount, clientInfo);
};

const register = async (serviceAccount, clientConfig, clientAppIdentifiers) => {
  const hasStoredClientData = !_.isEmpty(clientAppIdentifiers);

  let client;
  if (hasStoredClientData) {
    client = await fetchClientApp(serviceAccount, clientAppIdentifiers);
    if (!client) {
      logger.log('Could not find client, attempting registration...');
    }
  }
  if (client) {
    logger.info('fetched existing client:', client.name);
  } else {
    client = await attemptRegistration(serviceAccount, clientConfig);
    logger.info('registered service client:', client.name);
  }

  // set the client logo if appropriate
  if (clientConfig.logoPath) {
    try {
      await setLogo(serviceAccount, client._id, clientConfig.logoPath);
    } catch (e) {
      logger.error('setLogo error', e.message);
    }
  }

  const formatted = {
    ...client,
    redirectUris: clientConfig.redirectUris
  };
  return formatted;
};

module.exports = register;
