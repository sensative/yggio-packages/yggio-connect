'use strict';
// client/index.js

const _ = require('lodash');
const logger = require('../logger');
const {
  refreshAccessToken, subscribe, oauthCode
} = require('../routes');
const recursiveLogin = require('./recursive-login');
const registerClient = require('./register');
const {getClient} = require('../routes');

// all the mutable config stuff needed for a functioning client
let accountConfig = null;
let clientConfig = null;
let refreshCallback = null;
// store the details returned through the setup process.
const entities = {};

// keep the setup function separate from the setConfigs, since it should
// be possible to use the raw routes (i.e. without any client refresh-wrapping)
// if desired. But if we want to use the client, then all the information/stuffings
// need to be included.

let setupBegun = false;
let setupFinished = false;

// Initialization ensures the service is properly registered to Yggio.
// Depending on configuration, routes will be wrapped with your service client to enable
// automatic handling of tokens and client-dependent routes.
// Returns:
//  {
//    Client data, including secrets
//    Account information
//  }
const init = async (account, client, cb, clientAppIdentifiers) => {
  if (setupFinished || setupBegun) {
    // we keep track of setupBegun, since setup stuff is async
    throw new Error('client setup can only be run once');
  }
  setupBegun = true;

  accountConfig = account;
  clientConfig = client;
  refreshCallback = cb;

  entities.account = await recursiveLogin(accountConfig);
  try {
    entities.client = await registerClient(entities.account, clientConfig, clientAppIdentifiers);
  } catch (e) {
    if (e.statusCode !== 409) {
      logger.error('Could not register client:', _.get(e, 'response.data'));
      throw e;
    }
  }
  const secret = _.get(entities, 'client.secret', clientAppIdentifiers.secret);
  const provId = _.get(entities, 'client.client_id', clientAppIdentifiers.client_id);
  if (!secret) {
    throw new Error('No secret could be found when initializing client, please provide a secret or new client details');
  }
  _.set(entities, 'client.secret', secret);
  _.set(entities, 'client.client_id', provId);

  setupFinished = true;

  return entities;
};

// this is just a utility function to check if we have a refresh-error
const isRefreshError = err => {
  return err.statusCode === 401;
};

const initError = new Error('Client has not been initialized');
const wrapRoute = route => {
  if (!setupFinished) {
    throw initError;
  }
  return async (inputUser, ...args) => {
    // since we are using lodash, make sure we make a copy or toObject.
    // This way, we do not mutate the input object at all.
    const isModel = _.get(inputUser, 'toObject');
    const user = isModel ? inputUser.toObject() : Object.assign({}, inputUser);

    // extract the access token
    const originalAccessToken = _.get(user, 'accessToken');
    // extract the refresh token
    const refreshToken = _.get(user, 'refreshToken');
    // no matter what we run the route straight off
    try {
      return await route(user, ...args);
    } catch (err) {
      // if there's no accessToken , just throw the error
      if (!originalAccessToken) {
        throw err;
      }
      // if it's not a refresh-error, then we just throw that error
      if (!isRefreshError(err)) {
        throw err;
      }
      // if we do not have a refreshToken, then we cannot refresh
      if (!refreshToken) {
        logger.error('refresh token is missing');
        throw new Error('refresh token is missing');
      }
      // now we know that we do need to refresh, and we should nominally have sufficient information
      const tokens = await refreshAccessToken(entities.client, refreshToken);
      // allow the service to save the new accessToken (i.e. in user object)
      // and then rerun the original call
      const userCopy = Object.assign({}, user, tokens);
      await refreshCallback(user, tokens);
      return route(userCopy, ...args);
    }
  };
};

const wrappedSubscribe = async (user, yggioId, protocol, protocolData, name) => {
  if (!setupFinished) {
    throw initError;
  }

  const subscription = {
    name,
    iotnode: yggioId,
    [protocol]: protocolData
  };
  return wrapRoute(subscribe)(user, subscription);
};

const redeemCode = async (code, redirectUri) => {
  if (!setupFinished) {
    throw initError;
  }
  return oauthCode(entities.client, code, redirectUri);
};

const getClientDetails = () => {
  if (!setupFinished) {
    throw initError;
  }
  return entities;
};

module.exports = {
  init,
  // expose functionality to create the wrapped routes
  isInitialized: () => setupFinished,
  wrapRoute,
  // the rest get exposed directly to the outside
  getClientDetails,
  redeemCode,
  subscribe: wrappedSubscribe,
};
