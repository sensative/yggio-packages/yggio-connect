'use strict';
// yggio-request.js

const axios = require('axios');
const _ = require('lodash');

let yggioUrl = null;

const yggioRequest = async ({
  method,
  user,
  data,
  url,
  params,
  json = true,
  headers,
  formData
}) => {
  if (!yggioUrl) {
    throw new Error('yggioUrl is not set');
  }
  if (!url) {
    throw new Error('yggio api url is missing');
  }
  const accessToken = _.get(user, 'accessToken');

  const axiosData = _.pickBy({
    method,
    url: yggioUrl + url,
    params,
    data,
    json,
    headers: {
      ...headers,
      authorization: `Bearer ${accessToken}`
    },
    formData
  }, Boolean);

  const res = await axios(axiosData);
  return res.data;
};

module.exports = {
  setYggioUrl: url => {
    yggioUrl = url;
  },
  get: async requestObj => yggioRequest({
    ...requestObj,
    method: 'get'
  }),
  post: async requestObj => yggioRequest({
    ...requestObj,
    method: 'post'
  }),
  put: async requestObj => yggioRequest({
    ...requestObj,
    method: 'put'
  }),
  delete: async requestObj => yggioRequest({
    ...requestObj,
    method: 'delete'
  }),
  axios
};
