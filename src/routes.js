'use strict';
// routes.js
//
// This is just a flat file with a listing of yggio api commands,
// and should be treated as a reference/aid

const _ = require('lodash');
const fs = require('fs');
const assert = require('assert');
const axios = require('axios');
const Querystring = require('querystring');
const yggioRequest = require('./yggio-request');


// for token routes, we need to make sure that a token is supplied
const assertAccessToken = user => {
  assert(user && user.accessToken, 'The user access token is missing (user.accessToken)');
};

// ////
//
// Client section
//
// ////

let endpoints;
const getEndpoints = async () => {
  const urlPath = 'api/auth/endpoints';
  if (!endpoints) {
    endpoints = await yggioRequest.get({url: urlPath});
  }
  return endpoints;
};

const setLogo = async (user, clientId, logoPath) => {
  assertAccessToken(user);
  assert(clientId, 'clientId is missing');
  assert(logoPath, 'logoPath is missing');
  const url = 'api/client-apps/' + clientId + '/logo';
  const formData = {file: fs.createReadStream(logoPath)};
  return yggioRequest.post({user, url, formData});
};

const registerClient = async (user, client) => {
  assertAccessToken(user);
  const url = 'api/client-apps';
  const data = client;
  return yggioRequest.post({user, url, data});
};

const fetchClientApp = async (user, clientAppIdentifiers) => {
  assertAccessToken(user);
  const url = 'api/client-apps';
  const clientApps = await yggioRequest.get({user, url});
  return clientApps.find(c => c.client_id === clientAppIdentifiers.client_id);
};

const ensureChannel = async (user, clientId, newChannel) => {
  assertAccessToken(user);
  assert(newChannel, 'new channel data missing')
  const url = 'api/client-apps/' + clientId ;
  const data = {channel: newChannel};
  return yggioRequest.put({user, url, data});
};

// subscribes to a channel
const subscribe = async (user, data) => {
  assertAccessToken(user);
  assert(data.iotnode, 'invalid subscription');
  const url = 'api/channels';
  return yggioRequest.post({user, url, data});
};

// ////
//
// Channel section
//
// ////

const getChannels = async (user, iotnode) => {
  assertAccessToken(user);
  const url = 'api/channels';
  const params = {iotnode};
  return yggioRequest.get({user, params, url});
};

const getChannel = async (user, channelId) => {
  assertAccessToken(user);
  const url = 'api/channels/' + channelId;
  return yggioRequest.get({user, url});
};

// ////
//
// Locations section
//
// ////

const getLocations = async (user) => {
  assertAccessToken(user);
  const url = 'api/locations';
  return yggioRequest.get({user, url});
};

const updateLocation = async (user, locationId, data) => {
  assertAccessToken(user);
  const url = 'api/locations/' + locationId;
  return yggioRequest.put({user, url, data});
};

// ////
//
// Iotnodes section
//
// ////

const command = async (user, commandData) => {
  // TODO: give a description of allowed commandData structure
  assertAccessToken(user);
  const url = 'api/iotnodes/command';
  const data = commandData;
  return yggioRequest.put({user, url, data});
};

// Just like totally gets me
const getMe = async (user) => {
  assertAccessToken(user);
  const url = 'api/users/me';
  return yggioRequest.get({user, url});
};

const logoutFromYggio = async (user, userId) => {
  assertAccessToken(user);
  const url = 'api/users/' + userId + '/logout';
  return yggioRequest.post({user, url});
};

const getNode = async (user, nodeId) => {
  assertAccessToken(user);
  const url = 'api/iotnodes/' + nodeId;
  return yggioRequest.get({user, url});
};

const getNodes = async (user) => {
  assertAccessToken(user);
  const url = 'api/iotnodes';
  return yggioRequest.get({user, url});
};

const getAccessibleNodes = async (user) => {
  assertAccessToken(user);
  const url = 'api/iotnodes/shared';
  return yggioRequest.get({user, url});
};

const createNode = async (user, iotnode) => {
  assertAccessToken(user);
  const url = 'api/iotnodes';
  const data = iotnode;
  return yggioRequest.post({user, url, data});
};

const updateNode = async (user, nodeId, updates) => {
  assertAccessToken(user);
  const url = 'api/iotnodes/' + nodeId;
  const data = updates;
  return yggioRequest.put({user, url, data});
};

const deleteNode = async (user, nodeId) => {
  assertAccessToken(user);
  const url = 'api/iotnodes/' + nodeId;
  return yggioRequest.delete({user, url});
};

// ////
//
// OAuth
//
// ////

const checkOAuthStatus = async (user, nodeType) => {
  assertAccessToken(user);
  const url = 'api/oauth';
  const params = {nodeType};
  return yggioRequest.get({user, url, params});
};

// ////
//
// Rules
//
// ////

const getRule = async (user, ruleId) => {
  assertAccessToken(user);
  const url = 'api/rules/rules/' + ruleId;
  return yggioRequest.get({user, url});
};

const getRules = async (user) => {
  assertAccessToken(user);
  const url = 'api/rules/rules';
  return yggioRequest.get({user, url});
};

const executeRule = async (user, ruleId) => {
  assertAccessToken(user);
  const url = 'api/rules/rules/activate/' + ruleId;
  return yggioRequest.put({user, url});
};


//
//
// ARE THESE USED ??????? - they should at least be renamed
const getScenarioTemplates = async (user) => {
  assertAccessToken(user);
  const url = 'api/rules/templates';
  return yggioRequest.get({user, url});
};
const createScenario = async (user, templateId) => {
  assertAccessToken(user);
  // Apply template does not remove existing rules
  const url = 'api/rules/templates/' + templateId + '/apply';
  return yggioRequest.put({user, url});
};
//
// ARE THESE USED ???????
//
//

const getRulesConditions = async (user) => {
  assertAccessToken(user);
  const url = 'api/rules/conditions';
  return yggioRequest.get({user, url});
};

// TODO: this one should switch args order to (user, conditionId, updates)
const updateRulesCondition = async (user, updates, conditionId) => {
  assertAccessToken(user);
  const url = 'api/rules/conditions/' + conditionId;
  const data = updates;
  return yggioRequest.put({user, url, data});
};

const createRulesCondition = async (user, condition) => {
  assertAccessToken(user);
  const url = 'api/rules/conditions';
  const data = condition;
  return yggioRequest.post({user, url, data});
};

const getRulesActions = async (user) => {
  assertAccessToken(user);
  const url = 'api/rules/actions';
  return yggioRequest.get({user, url});
};

const createRulesAction = async (user, action) => {
  assertAccessToken(user);
  const url = 'api/rules/actions';
  const data = action;
  return yggioRequest.post({user, url, data});
};

// TODO: this one should have args signature (user, actionId, updates)
const updateRulesAction = async (user, updates) => {
  assertAccessToken(user);
  assert(updates && updates._id, 'invalid updates object');
  const actionId = updates._id;
  const url = 'api/rules/actions/' + actionId;
  const data = updates;
  return yggioRequest.put({user, url, data});
};

const deleteRulesAction = async (user, actionId) => {
  assertAccessToken(user);
  const url = 'api/rules/actions/' + actionId;
  return yggioRequest.delete({user, url});
};

const createRule = async (user, rule) => {
  assertAccessToken(user);
  const url = 'api/rules/rules';
  const data = rule;
  return yggioRequest.post({user, url, data});
};

const deleteRule = async (user, ruleId) => {
  assertAccessToken(user);
  const url = 'api/rules/rules/' + ruleId;
  return yggioRequest.delete({user, url});
};

// TODO: this one should have args signature (user, ruleId, updates)
const updateRule = async (user, updates) => {
  assertAccessToken(user);
  assert(updates && updates._id, 'invalid updates object');
  const ruleId = updates._id;
  const url = 'api/rules/rules/' + ruleId;
  const data = updates;
  return yggioRequest.put({user, url, data});
};

// TODO: to follow naming conventions above, this one should be called createRulesTrigger
const addRuleTrigger = async (user, ruleId, trigger) => {
  assertAccessToken(user);
  const url = 'api/rules/rules/' + ruleId + '/addtrigger';
  const data = trigger;
  return yggioRequest.put({user, url, data});
};
// TODO: where are delete and update RulesTrigger ?????

// ////////
// SURELY THIS ONE IS DEAD ?????
// TODO: to follow naming conventions above, this one should be called addRulesEvent
const addRuleEvent = async (user, ruleId, evt) => {
  assertAccessToken(user);
  const url = 'api/rules/rules/' + ruleId + '/addevent';
  const data = evt;
  return yggioRequest.put({user, url, data});
};
// NEEDS A SWIFT KICK TO THE HEAD
// ////////

// ////
//
// Contacts
//
// ////

const getContacts = async (user) => {
  assertAccessToken(user);
  const url = 'api/rules/contacts';
  return yggioRequest.get({user, url});
};

const setContacts = async (user, contacts) => {
  assertAccessToken(user);
  const url = 'api/rules/contacts';
  const data = contacts;
  return yggioRequest.put({user, url, data});
};

// ////
// ////
//
// NON-TOKEN ROUTES
//
// ////
// ////

const login = async ({username, password}) => {
  assert(username && password, 'username or password is missing');
  const url = 'api/auth/local';
  const data = {username, password};
  const {token} = await yggioRequest.post({url, data})
  return {accessToken: token};
};

const register = async ({username, password}) => {
  assert(username && password, 'username or password is missing');
  const url = 'api/users';
  const data = {username, password};
  return yggioRequest.post({url, data})
    .then(res => ({accessToken: res.token}));
};

const refreshAccessToken = async (client, refreshToken) => {
  assert(client, 'client is missing');
  assert(client.client_id, 'client.client_id is missing');
  assert(client.secret, 'client.secret is missing');
  assert(refreshToken, 'refreshToken is missing');

  const endpoints = await getEndpoints();
  const form = {
    grant_type: 'refresh_token',
    client_id: client.client_id,
    client_secret: client.secret,
    refresh_token: refreshToken
  };

  const data = Querystring.stringify(form);

  const res = await axios({
    method: 'post',
    url: endpoints.token_endpoint,
    data
  });

  const expiresAt = new Date(Date.now() + _.get(res, 'data.expires_in', 0) * 1000).toISOString();

  return {
    accessToken: _.get(res, 'data.access_token'),
    refreshToken: _.get(res, 'data.refresh_token'),
    expiresAt
  };
};

const oauthCode = async (client, code, redirectUri) => {
  assert(client, 'client is missing');
  assert(client.client_id, 'client.client_id is missing');
  assert(client.secret, 'client.secret is missing');
  assert(code, 'oauth code is missing');
  assert(redirectUri, 'redirectUri is missing');

  const endpoints = await getEndpoints();
  const form = {
    code,
    grant_type: 'authorization_code',
    redirect_uri: redirectUri,
    client_id: client.client_id,
    client_secret: client.secret,
    scope: 'openid email',
  };

  const data = Querystring.stringify(form);

  const res = await axios({
    method: 'post',
    url: endpoints.token_endpoint,
    data
  });

  const expiresAt = new Date(Date.now() + _.get(res, 'data.expires_in', 0) * 1000).toISOString();
  const user = {
    accessToken: _.get(res, 'data.access_token'),
    refreshToken: _.get(res, 'data.refresh_token'),
    idToken: _.get(res, 'data.id_token'),
    expiresAt,
    scope: _.get(res, 'data.scope')
  };

  const me = await getMe(user);
  return {
    ...user,
    ...me
  };
};

// ////
//
// //////// ALL ROUTES ////////
//
// ////

const nonTokenRoutes = {
  login,
  register,
  getEndpoints,
  refreshAccessToken,
  oauthCode,
};

const tokenRoutes = {
  // client
  setLogo,
  fetchClientApp,
  registerClient,
  subscribe,
  // channels
  getChannels,
  getChannel,
  ensureChannel,
  //locations
  getLocations,
  updateLocation,
  // iotnodes
  command,
  getMe,
  logoutFromYggio,
  getNode,
  getNodes,
  getAccessibleNodes,
  createNode,
  updateNode,
  deleteNode,
  // oauth
  checkOAuthStatus,
  // rules
  getRule,
  getRules,
  executeRule,
  getScenarioTemplates,
  createScenario,
  getRulesConditions,
  updateRulesCondition,
  createRulesCondition,
  getRulesActions,
  createRulesAction,
  updateRulesAction,
  deleteRulesAction,
  createRule,
  deleteRule,
  updateRule,
  addRuleTrigger,
  addRuleEvent,
  // Contacts
  getContacts,
  setContacts,
};

module.exports = {
  ...nonTokenRoutes,
  ...tokenRoutes
};
